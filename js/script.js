
function loadData() {

    var $body = $('body');
    var $wikiElem = $('#wikipedia-links');
    var $nytHeaderElem = $('#nytimes-header');
    var $nytElem = $('#nytimes-articles');
    var $greeting = $('#greeting');

    // clear out old data before new request
    $wikiElem.text("");
    $nytElem.text("");

    // load streetview

    // YOUR CODE GOES HERE!
    var street = $('#street').val();
    var city = $('#city').val();
    $body.append('<img class="bgimg" src="https://maps.googleapis.com/maps/api/streetview?size=600x300&location=' + street + ', ' + city + '">');
    
    //http://api.nytimes.com/svc/search/v2/articlesearch.response-format?[q=search term&fq=filter-field:(filter-term)&additional-params=values]&api-key=####
    var nytURL = "http://api.nytimes.com/svc/search/v2/articlesearch.json?[q="+city+'&fq=source:("The New York Times")&sort=newest&api-key=2619b62b09fea02311dad29062567031:12:73234295';
    $.getJSON( nytURL, function( data ) {
        var response = data.response.docs;
        for (i in response) {
            var pretitle = response[i].headline.name || response[i].headline.main;
            var title = '<a href='+response[i].web_url+'>'+ pretitle +'</a>';
            var snippet = '<p>'+response[i].snippet+'</p>';
            var article = '<li class="article">'+title+snippet+'</li>';
            $('#nytimes-articles').append(article);
        }
    }).error(function() {
        var errorMessage = "NYT articles couldn't be loaded.";
        $('#nytimes-articles').append(errorMessage);
    });

    return false;
};

$('#form-container').submit(loadData);
